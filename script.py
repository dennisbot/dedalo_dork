#!/usr/bin/python
# -*- coding: utf-8 -*-

import argparse
from argparse import RawTextHelpFormatter
import sys
import re
import codecs

import requests



def do_search(domain, subject, filetype):
    dork = "filetype:ppt"
    print filetype
    print dork
    if filetype != '':
        dork = "filetype:" + filetype

    url = "http://www.google.com/search"
    query = "site:" + domain + " " + dork
    query += " " + subject

    payload = {
        'hl': 'en',
        'q': query,
        'safe': 'off',
        'filter': 0,
            }

    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.6) Gecko/20070725 Firefox/2.0.0.6',
            }

    r = requests.get(url, params=payload, headers=headers)
    data = r.text

    #f = codecs.open("output.txt", "w", "utf-8")

    r1 = re.compile('<h3 class=[^>]+><a href="([^"]+)"')
    res = r1.findall(data)
    if len(res) > 0:
        out = "Showing just 5 first results for dork " + subject + "\n"
        for item in res[:12]:
            url=item.split('&')[0]
            url=url.split('://')[1]
            out += str(url) + "\n"

    print out
    #f.write(out)
    #f.close()

def main():
    description = """Do a Google search for files of specific content from
    University websites"""
    parser = argparse.ArgumentParser(description=description,formatter_class=RawTextHelpFormatter)
    parser.add_argument('-d', '--domain', action='store',
            metavar='www.domainUniversity.edu',
            help='URL for university website',
            required=True, dest='domain')
    parser.add_argument('-s', '--subject', action='store',
            help='keywords to look for [tema de búsqueda]',
            required=True, dest='subject')

    parser.add_argument('-f', '--filetype', action='store',
            help='el tipo de archivo que queremos[filetype]',
            required=True, dest='filetype')

    args = parser.parse_args()

    domain = args.domain.strip()
    subject = args.subject.strip()
    #filetype = "ppt"
    filetype = args.filetype.strip()


    do_search(domain, subject, filetype)

if __name__ == "__main__":
    main()
