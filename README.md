# Dependencias

    sudo pip install requests

# Ejecutar en command line

    python script.py -d www.unmsm.edu.pe -s ciencia

# usar como módulo

    from script import do_search
    do_search("www.unmsm.edu.pe", "ciencia", "ppt")
